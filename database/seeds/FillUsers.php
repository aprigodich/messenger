<?php

use Illuminate\Database\Seeder;

class FillUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create user
        DB::table('users')->insert([
            [
                'name' => 'Юзер',
                'username' => 'user',
                'email' => 'user@user.ru',
                'password' => bcrypt('user'),
            ],
            [
                'name' => 'Юзер1',
                'username' => 'user1',
                'email' => 'user1@user.ru',
                'password' => bcrypt('user1'),
            ],
            [
                'name' => 'Юзер2',
                'username' => 'user2',
                'email' => 'user2@user.ru',
                'password' => bcrypt('user2'),
            ],
            [
                'name' => 'Юзер3',
                'username' => 'user3',
                'email' => 'user2@user.ru',
                'password' => bcrypt('user3'),
            ],
            [
                'name' => 'Юзер4',
                'username' => 'user4',
                'email' => 'user2@user.ru',
                'password' => bcrypt('user4'),
            ],
        ]);
    }
}
