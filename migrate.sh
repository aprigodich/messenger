#!/bin/bash
composer dump-autoload
php artisan migrate:rollback
php artisan migrate
php artisan db:seed --class=DatabaseSeeder