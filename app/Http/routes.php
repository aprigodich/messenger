<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['prefix' => 'message'  ,/*'middleware' => 'auth'*/], function (){
    Route::get('/', ['as' => 'message.index', 'uses' => 'MessageController@index']);
    Route::post('/', ['as' => 'message.index', 'uses' => 'MessageController@index']);
    Route::post('/create', ['as' => 'message.create', 'uses' => 'MessageController@create']);
});

Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
Route::post('/auth/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);
Route::post('/auth/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout']);
Route::post('/get/user', ['as' => 'get.user', 'uses' => 'UserController@getUser']);

