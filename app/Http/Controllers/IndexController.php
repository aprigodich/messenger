<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller{

    public function index(){

        if(Auth::check()){
            $message = Message::where('is_read', '=', 0)
            ->where('to_who_sended', '=', Auth::user()->username)
                ->get()
                ->all();

            return View::make('main')->with([
                'messagesIsNotRead' => count($message),
                'message' => $message
            ]);
        }else{
            return View::make('main');
        }

    }

}