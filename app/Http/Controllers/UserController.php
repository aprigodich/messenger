<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getUser()
    {
        $query = file_get_contents('php://input');
        $query = json_decode($query) . '%';
        $user =  User::where('username', 'LIKE', $query)
                            ->where('username', '!=', Auth::user()->username)
                            ->take(5)
                            ->get();
        return $user;
    }
}
