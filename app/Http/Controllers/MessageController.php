<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class MessageController  extends Controller{

    public function index(){

        if(Auth::check()){
            if(isset(getallheaders()['Content-Type']) && getallheaders()['Content-Type'] === 'text/plain'){
                $messagesNotRead = Message::where('to_who_sended', '=', Auth::user()->username)->where('is_read', '=', 0)->get()->first();
                if($messagesNotRead){
                    $messagesNotRead->is_read = 1;
                    $messagesNotRead->save();
                }
                return $messagesNotRead;

            }

            $messages = Message::where('to_who_sended', '=', Auth::user()->username)->get()->all();
            if(!empty($messages)){
                foreach ($messages as $message){
                    if($message->is_read === '0'){
                        $message->is_read = 1;
                        $message->save();
                    }
                }
            }

            return View::make('message.index')->with([
                'messagesIsNotRead' => 0,
                'messages' => $messages
            ]);
        }else{
            abort(404, 'Permission');
        }

    }

    public function create(){
        if(Auth::check()) {
            if (isset(getallheaders()['Content-Type']) && getallheaders()['Content-Type'] === 'text/plain') {
                $query = $this->getContent();
                return Message::create([
                    'message' => $query->message,
                    'to_who_sended' => $query->user,
                    'from_who_sended' => Auth::user()->username,
                ]);
            }
        }else{
            abort(404, 'Permission');
        }
    }

    public function getContent(){
        return (json_decode(file_get_contents('php://input')));
    }

}