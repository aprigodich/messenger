<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Card
 * @package App
 */
class Message extends Model
{
    /**
     * @var string
     */

    protected $table = 'messages';
    /**
     * @var array
     */
    public $timestamps = true;

    protected $fillable = [
        'message',
        'is_read',
        'to_who_sended',
        'from_who_sended',
    ];


}


