<!DOCTYPE html>

<html>
    <head>
        <title>Messenger</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        {{--<script type="javascript" src="../../assets/js/message/GetUser.js"></script>--}}
    </head>

    @extends('parts.navbar')<br><br>

    <body>

    @yield('content')
    </body>
</html>
