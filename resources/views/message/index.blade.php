@extends('layout/layout')
@section('content')

    <div id = "messages">
        @foreach($messages as $message)
            <div class="col-lg-12">
                <h2>Message from: {{$message->from_who_sended}}</h2>
                <p>{{$message->message}}</p>
            </div>
        @endforeach
    </div>

    <div class="jumbotron" onclick="hideMultiple()">
        <div class="container">
            <h1>Send Message</h1>
            <div class="form-group" style="max-width:20%;">
                <label for="user">Enter User Name:</label>
                <input type="text" class="form-control" id="user" onkeyup="getUsersAjax()">
                <select multiple hidden id="select" style="width:230px;" onclick="getUserValue(event)">
                </select>
            </div>

            <div class="form-group">
                <label for="message">Message:</label>
                <textarea class="form-control" rows="5" id="message" style="max-width:100%;"></textarea>
            </div>
            <button type="submit" onclick="sendMessage()" class="btn btn-success">Send</button>
            <button type="show" onclick="getMessage()" class="btn btn-success">Send</button>
            <audio src="/mp3/sms.mp3" id="audio"  ></audio>
        </div>

    </div>

    <script>

        function focus()
        {
            document.getElementById("message").focus();
        }
        window.onload = function () {
            setInterval(getMessage, 1000);
            focus();
        };

        var user = document.getElementById('user');
        var select = document.getElementById('select');
        var message = document.getElementById('message');


        function hideMultiple() {
            select.hidden = true;
            while (select.childNodes.length){
                select.removeChild(select.lastChild);
            }
        }

        function getUserValue(event) {
            user.value = event.target.text;
        }


        function getUsersAjax() {

            while (select.childNodes.length){
                select.removeChild(select.lastChild);
            }

            var option = document.createElement('option');
            var req = ajaxPost('/get/user', user.value);
            req.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var response = JSON.parse(this.response);
                    if(response.length > 0 && user.value.length > 0){
                        select.hidden = false;
                        for(var i = 0; i < response.length; i++){
                            var option = document.createElement('option');
                            option.appendChild(document.createTextNode(response[i].username));
                            select.appendChild(option.cloneNode(true));
                        }
                    }else{
                        select.hidden = true;
                    }
                }
            };
        }

        function sendMessage() {

            while (select.childNodes.length){
                select.removeChild(select.lastChild);
            }

            var data = {'user' : user.value, 'message' : message.value};

             ajaxPost('/message/create', data);
        }


        function getMessage() {
            var messages = document.getElementById('messages');
            var h2 = document.createElement('h2');
            var p = document.createElement('p');
            var div = document.createElement('div');
            var audio = document.getElementById('audio');
            var req = ajaxGet('/message');
            req.onreadystatechange = function() {
                console.log(req.response);
                if (this.readyState == 4 && this.status == 200 && req.response.length > 0) {
                    var response = JSON.parse(this.response);
                    div.setAttribute('class' , 'col-lg-12');
                    h2.textContent = 'Message from: ' + response.from_who_sended;
                    p.textContent = response.message;
                    div.appendChild(h2);
                    div.appendChild(p);
                    messages.appendChild(div);
                    audio.play();
                }
            };
        }

        function ajaxGet(url) {
            var token = document.getElementsByName('_token')[0].value;
            var req = new XMLHttpRequest();
            req.open('GET', url, true);
            req.setRequestHeader('Content-Type', 'text/plain');
            req.setRequestHeader('X-CSRF-Token', token);
            req.send();
            return req;
        }

        function ajaxPost(url, data) {
            var token = document.getElementsByName('_token')[0].value;
            var req = new XMLHttpRequest();
            req.open('POST', url, true);
            req.setRequestHeader('Content-Type', 'text/plain');
            req.setRequestHeader('X-CSRF-Token', token);
            req.send(JSON.stringify(data));
            return req;
        }
    </script>
@endsection