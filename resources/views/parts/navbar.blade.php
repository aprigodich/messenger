
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Messenger</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            @if(!auth()->user())
                <form method="post" action="{{route('auth.login')}}" class="navbar-form navbar-right">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" placeholder="Username" name="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" name="password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            @endif
            @if(auth()->user())
                <form method="post" action="{{route('auth.logout')}}" class="navbar-form navbar-right">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Logout</button>
                </form>
                    <form  action="{{route('message.index')}}" class="navbar-form navbar-right">

                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-envelope" aria-hidden="true">
                            </span>
                            <span class="glyphicon-class">{{$messagesIsNotRead}}</span>
                        </button>
                    </form>
            @endif
        </div><!--/.navbar-collapse -->
    </div>
</nav>